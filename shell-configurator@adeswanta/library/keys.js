/**
 * SCKeys
 * 
 * @Description      Keys manager for prefs settings
 * @Filename         keys.js
 * @License          GNU General Public License v3.0
 */

var SCKeys = class {
    constructor() {
        this.keys = {};
    }

    // addCategory: Create category configuration item
    addCategory(name, display, configName, description) {
        if (!this.keys.hasOwnProperty(name)) {
            this.keys[name] = {
                'category': name,
                'displayName': display,
                'configName': configName,
                'description': description,
                'configs': {}
            }
        }
    }

    // addKey: Create the preferences key object.
    addKey(category, name, display, description, type, supported, data, widget) {
        // Use for widget/configuration id
        let id = category + '-' + name.replace(/-/g, "_");
        this.keys[category]["configs"][id] = {
            'id': id, // Key ID
            'name': name, // Configuration name
            'display': display, // Configuration display name (ex: Panel size)
            'description': description, // Configuration description
            'category': category, // Configuration category
            'schemaID': (category + '-' + name).toLowerCase(), // Schema name
            'type': type, // Configuration type
            'supported': supported, // Configuration compatibility
            'data': data, // Configuration data
            'widget': widget // Control Widget
        }
    }

    // resetEmpty: Removes all SCKeys
    resetEmpty() {
        if (this.keys != {}) {
            // Set to null object
            this.keys = {};
        }
    }

    // getKeys: Gets all settings keys that saved to 'keys' object (publicly)
    getKeys() {
        // Return the keys
        return this.keys;
    }
}