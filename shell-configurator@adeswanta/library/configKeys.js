// Translations
const Gettext = imports.gettext;
const Domain = Gettext.domain(Self.metadata['gettext-domain']);
const _ = Domain.gettext;

// Define current GNOME Shell version
const shellVersion = parseFloat(imports.misc.config.PACKAGE_VERSION);

var keys = [
    {
        category: "panel",
        displayName: _("Panel"),
        configName: "configPanel",
        description: _("Your system control on the top of display"),
        configs: [
            [
                'visibility', _('Visibility'),
                _("Show/Hide the panel"),
                'boolean', true, []
            ],
            [
                'auto-hide', _('Auto Hide'),
                _("Automatically hides the panel and show when cursor entered the panel"),
                'boolean', true, []
            ],
            [
                'height', _('Height'),
                _("Height size of the panel"),
                'int', true, [16, 128, 1]
            ],
            [
                'position', _('Position'),
                _("Move the panel position in vertical"),
                'enum', true,
                [
                    ["TOP", _("Top (Default)")],
                    ["BOTTOM", _("Bottom")]
                ]
            ],
            [
                'hot-corner', _('Hot Corner'),
                _("Enable hot corner for triggering Activities Button"),
                'boolean', true, []
            ],
        ]
    },
    {
        category: "dash",
        displayName: _("Dash"),
        configName: "configDash",
        description: _("Launches an application and favorites"),
        configs: [
            [
                'visibility', _('Visibility'),
                _("Show/Hide Dash"),
                'boolean', true, []
            ]
        ]
    },
    {
        category: "overview",
        displayName: _("Overview"),
        configName: "configOverview",
        description: _("Organize workspace and windows"),
        configs: [
            [
                'workspace-switcher-visibility', _('Workspace Switcher Visibility'),
                _("Show/Hide Workspace switcher on the right/top of overview"),
                'boolean', true, []
            ],
            [
                'workspace-switcher-peak-width', _('Workspace Switcher Peak Width'),
                _("Peak width size of Workspace Switcher when it's in idle state"),
                'int', shellVersion <= 3.38, [-1, 96, 1]
            ],
            [
                'workspace-switcher-scale-size', _('Workspace Switcher Scale Size'),
                _("Maximum scale size of Workspace Switcher on the top of overview"),
                'int', shellVersion >= 40, [0, 10, 1]
            ]
        ]
    },
    {
        category: "search",
        displayName: _("Search"),
        configName: "configSearch",
        description: _("The universal search from GNOME that easily search everything in computer"),
        configs: [
            [
                'visibility', _('Visibility'),
                _("Show/Hide search entry on the top of overview"),
                'boolean', true, []
            ],
            [
                'type-to-search', _('Type to search behavior'),
                _("Triggers start search when user type on overview"),
                'boolean', true, []
            ]
        ]
    },
    {
        category: "appGrid",
        displayName: _("App Grid"),
        configName: "configAppGrid",
        description: _("Collection of Applications"),
        configs: [
            [
                'rows', _('Row Size'),
                _("Row page size of App Grid"),
                'int', true, [2, 12, 1]
            ],
            [
                'columns', _('Column Size'),
                _("Column page size of App Grid"),
                'int', true, [2, 12, 1]
            ]
        ]
    },
    {
        category: "lookingGlass",
        displayName: _("Looking Glass"),
        configName: "configLookingGlass",
        description: _("Inspecting shell elements and debugging"),
        configs: [
            [
                'horizontal-position', _('Horizontal Position (in %)'),
                _("Horizontal position of Looking Glass in percentage value"),
                'int', true, [0, 100, 1]
            ],
            [
                'vertical-position', _('Vertical Position'),
                _("Vertical position of Looking Glass"),
                'enum', true,
                [
                    ["TOP", _("Top (Default)")],
                    ["BOTTOM", _("Bottom")]
                ]
            ],
            [
                'width', _('Width Size'),
                _("Width size of Looking Glass"),
                'int', true, [0, 2048, 1]
            ],
            [
                'height', _('Height Size'),
                _("Height size of Looking Glass"),
                'int', true, [0, 2048, 1]
            ]
        ]
    }
]