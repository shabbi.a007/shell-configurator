/**
 * SCComponents
 * 
 * @Description      Shell components for configuring shell properties and make it compatible
 * @Filename         components.js
 * @License          GNU General Public License v3.0
 */

const Self = imports.misc.extensionUtils.getCurrentExtension();
const {
    panel:Panel,
    dash:Dash,
    overview:Overview,
    search:Search,
    app_grid:AppGrid,
    looking_glass:LookingGlass
} = Self.imports.library.modules;

var SCComponents = class {
    constructor() {
        this.Panel = new Panel.Panel();
        this.Dash = new Dash.Dash();
        this.Overview = new Overview.Overview();
        this.Search = new Search.Search();
        this.AppGrid = new AppGrid.AppGrid();
        this.LookingGlass = new LookingGlass.LookingGlass();
    }

    backup() {
        this.Panel.backup();
        this.Dash.backup();
        this.Overview.backup();
        this.Search.backup();
        this.AppGrid.backup();
        this.LookingGlass.backup();
    }

    restore() {
        this.Panel.restore();
        this.Dash.restore();
        this.Overview.restore();
        this.Search.restore();
        this.AppGrid.restore();
        this.LookingGlass.restore();
    }
}