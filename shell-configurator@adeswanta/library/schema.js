/**
 * SCSchema
 * 
 * @Description      Settings schema manager
 * @Filename         schema.js
 * @License          GNU General Public License v3.0
 */

// Import Gio libraries from gi
const { Gio } = imports.gi;

// newSchema: Create new Gio Settings from default schemas folder (/usr/share/glib-2.0/schemas)
function newSchema(schemaPath) {
    // Returning Gio.Settings with default schema source
    return new Gio.Settings({
        schema_id: schemaPath
    });
}

// newSchemaFromSource: Create new Gio Settings from custom schema source
function newSchemaFromSource(sourcePath, schemaPath) {
    // Add custom schema source path
    let schemaSource = Gio.SettingsSchemaSource.new_from_directory(
        sourcePath, // 'schemas' directory path
        Gio.SettingsSchemaSource.get_default(), // parent
        false // trusted schema
    );
    // Returning Gio.Settings with custom schema source
    return new Gio.Settings({ 
        settings_schema : schemaSource.lookup(schemaPath, true) 
    });
}