/**
 * SCPrefs
 *
 * @Description      The preferences library for Shell Configurator settings
 * @Filename         preferences.js
 * @License          GNU General Public License v3.0
 */

const Self = imports.misc.extensionUtils.getCurrentExtension();

// Import gi libraries
const { Gtk, Gdk, GLib, GObject, Gio } = imports.gi;

// Translations
const Gettext = imports.gettext;
const Domain = Gettext.domain(Self.metadata['gettext-domain']);
const _ = Domain.gettext;

// Global variables
let isFocusWithinEntry = false;

var SCPrefs = class {
    constructor(Self, Libs, extensionSettings, shellVersion) {
        this._Self = Self;

        this._Keys = Libs['Keys'];
        this._Misc = Libs['Misc'];

        this._extensionSettings = extensionSettings;
        this._shellVersion = shellVersion;

        this._configKeys = Self.imports.library.configKeys.keys;

        this._builder = new Gtk.Builder();
        this._releaseTypeLabel = "";

        this._searchWidgetResult = [];
        this._searchResultCategory = [];

        this._presetCombo = null;

        this._url = {
            'repository': 'https://gitlab.com/adeswantaTechs/shell-configurator',
            'references': 'https://gitlab.com/adeswantaTechs/shell-configurator/-/blob/master/REFERENCES.md',
            'bug_report': 'https://gitlab.com/adeswantaTechs/shell-configurator/-/issues'
        };
        this._presets = {
            "default": "Default",
            "minimal": "Minimal"
        };

        this._createUITemplate();
    }

    // _createUITemplate: Create our custom GTK Widgets for ui preferences
    _createUITemplate() {
        // Configuration item
        this.configItem = GObject.registerClass({
            GTypeName: 'configItem',
            Template: `file://${this._Self.dir.get_child('ui').get_path()}/config-item.ui`,
            InternalChildren: ['configName', 'configDescription', 'configSuffix']
        }, class configItem extends Gtk.Box {
            _init(key) {
                super._init({
                    name: key.id
                });
                this._configName.set_label(key.display);
                this._configDescription.set_label(key.description);
                switch (key.type) {
                    case 'boolean':
                        this._configControl = new Gtk.Switch({name: key.id + '-state'});
                        break;

                    case 'int':
                        this._adjustment = new Gtk.Adjustment({
                            lower: key.data[0],
                            upper: key.data[1],
                            step_increment: key.data[2]
                        });
                        this._configControl = new Gtk.SpinButton({
                            name: key.id + '-size',
                            adjustment: this._adjustment
                        });
                        // Connect widget to emit focus within of entry (for GTK+ 3 only)
                        let intFocusInID, intFocusOutID;
                        if (Gtk.MAJOR_VERSION == 3) {
                            intFocusInID = this._configControl.connect('focus-in-event', () => {
                                isFocusWithinEntry = true;
                            })
                            intFocusOutID = this._configControl.connect('focus-out-event', () => {
                                isFocusWithinEntry = false;
                            })
                        }
                        this._configControl.connect('destroy', () => {
                            this._configControl.disconnect(intFocusInID);
                            this._configControl.disconnect(intFocusOutID);
                        })
                        if (Gtk.MAJOR_VERSION == 4)
                            this._configControl.add_css_class('flat')
                        else 
                            this._configControl.get_style_context().add_class('flat');
                        break;

                    case 'string':
                        this._configControl = new Gtk.Entry({ name: key.id + '-value' });
                        // Connect widget to emit focus within of entry (for GTK+ 3 only)
                        let strFocusInID, strFocusOutID;
                        if (Gtk.MAJOR_VERSION == 3) {
                            strFocusInID = this._configControl.connect('focus-in-event', () => {
                                isFocusWithinEntry = true;
                            })
                            strFocusOutID = this._configControl.connect('focus-out-event', () => {
                                isFocusWithinEntry = false;
                            })
                        }
                        this._configControl.connect('destroy', () => {
                            this._configControl.disconnect(strFocusInID);
                            this._configControl.disconnect(strFocusOutID);
                        })
                        break;

                    case 'enum':
                        this._configControl = new Gtk.ComboBoxText({ name: key.id + '-item' });
                        for (let item of key.data) {
                            this._configControl.append(item[0], item[1]);
                        }
                        if (Gtk.MAJOR_VERSION == 4) {
                            // Make it flat (for GTK 4)
                            let toggleButton = this._configControl.get_first_child().get_first_child()
                            toggleButton.add_css_class('flat')
                        }
                        break;
                }
                this._configControl.set_visible(true);
                if (Gtk.MAJOR_VERSION == 4)
                    this._configSuffix.append(this._configControl);
                else
                    this._configSuffix.pack_start(this._configControl, false, true, 0);
            }
        });

        this.modulePage = GObject.registerClass({
            GTypeName: 'modulePage',
            Template: `file://${this._Self.dir.get_child('ui').get_path()}/module-page.ui`,
            InternalChildren: ['moduleTitle', 'moduleDescription', 'configs']
        }, class modulePage extends Gtk.Box {
            _init(id, title, description) {
                super._init({
                    name: id
                });
                this._moduleTitle.set_label(title);
                this._moduleDescription.set_label(description);
            }
        });

        this.searchResultItem = GObject.registerClass({
            GTypeName: 'searchResultItem',
            Template: `file://${this._Self.dir.get_child('ui').get_path()}/search-result-item.ui`,
            InternalChildren: ['configName', 'configLocation']
        }, class searchResultItem extends Gtk.Box {
            _init(name, location) {
                super._init();
                this._configName.set_label(name);
                this._configLocation.set_label(location);
            }
        });
    }

    // _isDevelopment: Check if this extension is in development
    _isDevelopment() {
        if (this._Self.metadata['release-state'] === 'development') {
            this._releaseTypeLabel = `\n(${_("Development Release")})`;
            return true;
        }
        this._releaseTypeLabel = "";
        return false;
    }

    // _addKeys: Add the settings key that are save to keys object from keys.js
    _addKeys() {
        for (let category of this._configKeys) {
            this._Keys.addCategory(
                category.category,
                category.displayName,
                category.configName,
                category.description
            );
            for (let config of category.configs) {
                this._Keys.addKey(
                    category.category,
                    config[0],  // name/id
                    config[1],  // displayName
                    config[2],  // description
                    config[3],  // type
                    config[4],  // supported
                    config[5],  // data
                    null        // widget
                );
            }
        }
    }

    // _checkPreset: Check the preset name from these configurations
    _checkPreset() {
        let total = 0;
        let match = {};
        let matchID = "custom";

        for (let preset of Object.keys(this._presets)) {
            match[preset] = 0;
        }

        for (let category of Object.values(this._Keys.getKeys())) {
            for (let key of Object.values(category["configs"])) {
                if (key.supported) {
                    let value;
                    switch (key.type) {
                        case 'boolean':
                            value = key.widget.get_active();
                            break;
                        case 'int':
                            value = key.widget.get_value_as_int();
                            break;
                        case 'enum':
                            if (this._shellVersion >= 42) {
                                for (let item of key.data) {
                                    if (key.widget.get_selected_item().string == item[1]) {
                                        value = item[0];
                                    }
                                }
                            }
                            else {
                                value = key.widget.get_active_id();
                            }
                            break;
                    }

                    for (let preset of Object.keys(this._presets)) {
                        let configsFile = Gio.File.new_for_path(GLib.build_filenamev([
                            this._Self.dir.get_child('presets').get_path(), `${preset}.json`
                        ]));
                        let [ok, contents, etag] = configsFile.load_contents(null);
                        let configs = JSON.parse(this._Misc.byteArrayToString(contents));

                        let isMatch = (value == configs["configs"][category.category][key.name])
                        match[preset] += isMatch ? 1 : 0;
                    }
                    total++;
                }
            }
        }

        for (let preset of Object.keys(this._presets)) {
            matchID = (match[preset] == total) ? preset : matchID;
        }

        this._presetCombo.set_active_id(matchID)
    }

    // _setPreset: Set the config preset from available presets
    _setPreset(name) {
        // No custom preset
        if (name == "custom") { return }

        let configsFile = Gio.File.new_for_path(GLib.build_filenamev([
            this._Self.dir.get_child('presets').get_path(), `${name}.json`
        ]));
        let [ok, contents, etag] = configsFile.load_contents(null);
        let configs = JSON.parse(this._Misc.byteArrayToString(contents));

        if (ok) {
            for (let category of Object.values(this._Keys.getKeys())) {
                for (let key of Object.values(category["configs"])) {
                    if (key.supported) {
                        switch (key.type) {
                            case 'boolean':
                                this._extensionSettings.set_boolean(
                                    key.schemaID, configs["configs"][category.category][key.name]
                                );
                                break;
                            case 'int':
                                this._extensionSettings.set_int(
                                    key.schemaID, configs["configs"][category.category][key.name]
                                );
                                break;
                            case 'enum':
                                this._extensionSettings.set_enum(
                                    key.schemaID, configs["configs"][category.category][key.name]
                                );
                                break;
                        }
                    }
                }
            }
        }
    }

    // _searchConfig; Search function for searching configuration
    _searchConfig(data = "") {
        // option result list widget
        let configResult = this._builder.get_object("configResult")
        // Remove previous result data
        if (Gtk.MAJOR_VERSION == 4) {
            for (let item of this._searchWidgetResult) {
                configResult.remove(item);
            }
        } else {
            for (let item of configResult.get_children()) {
                configResult.remove(item);
            }
        }
        this._searchWidgetResult = [];
        this._searchResultCategory = [];

        let index = 0

        for (let category of Object.values(this._Keys.getKeys())) {
            for (let key of Object.values(category["configs"])) {
                let availableValidation =
                    data == "" || key.supported && (
                        // The item will shown if config is supported and config data (id, display,
                        // and description) is match/included with substring of search data
                        key.id.replace('-', ' ').toLowerCase().includes(data.toLowerCase()) ||
                        key.display.toLowerCase().includes(data.toLowerCase()) ||
                        key.description.toLowerCase().includes(data.toLowerCase())
                    );
                if (availableValidation) {
                    // create an item
                    let container = new this.searchResultItem(
                        key.display,
                        `${category.displayName}`
                    )
                    if (Gtk.MAJOR_VERSION == 4) {
                        let item = new Gtk.ListBoxRow({
                            selectable: false
                        });
                        item.set_child(container);
                        configResult.append(item);
                        this._searchWidgetResult.push(item);
                    } else {
                        configResult.insert(container, index);
                        this._searchWidgetResult.push(container);
                        index++;
                    }
                    if (key.widget) {
                        // Used to focus the row (widget -> box -> row)
                        container._row = key.widget.get_parent().get_parent().get_parent();
                    }

                    // Push result data
                    this._searchResultCategory.push(category.configName);
                }
            }
        }
        // Hide config result list if result data is empty
        configResult.get_parent().set_visible(!this._searchWidgetResult.length == 0)
    }

    // _connectSignals: Connect settings signals when user triggering widget signals on prefs
    _connectSignals(window) {
        // Connect configuration signals
        for (let category of Object.values(this._Keys.getKeys())) {
            for (let key of Object.values(category["configs"])) {
                if (key.supported) {
                    switch (key.type) {
                        case 'boolean':
                            this._extensionSettings.bind(
                                key.schemaID,
                                key.widget, 'active',
                                Gio.SettingsBindFlags. DEFAULT
                            );
                            if (key.id === "panel-visibility") {
                                key.widget.connect("state-set", (out) => {
                                    this._Keys.getKeys().panel.configs["panel-height"].widget
                                    .set_sensitive(out.get_active());
                                    this._Keys.getKeys().panel.configs["panel-auto_hide"].widget
                                    .set_sensitive(out.get_active());
                                    this._checkPreset();
                                })
                            } else if (key.id === "panel-auto_hide") {
                                key.widget.connect("state-set", (out) => {
                                    this._Keys.getKeys().panel.configs["panel-visibility"].widget
                                    .set_sensitive(!out.get_active());
                                    this._checkPreset();
                                })
                            } else {
                                key.widget.connect("state-set", this._checkPreset.bind(this))
                            }
                            break;
                        case 'int':
                            this._extensionSettings.bind(
                                key.schemaID,
                                key.widget, 'value',
                                Gio.SettingsBindFlags. DEFAULT
                            );
                            key.widget.connect("value-changed", this._checkPreset.bind(this))
                            break;
                        case 'enum':
                            if (this._shellVersion >= 42) {
                                key.widget.connect("notify::selected-item", (out) => {
                                    this._extensionSettings.set_enum(key.schemaID, out.get_selected());
                                })
                            } else {
                                this._extensionSettings.bind(
                                    key.schemaID,
                                    key.widget, 'active-id',
                                    Gio.SettingsBindFlags. DEFAULT
                                );
                            }
                            key.widget.connect("notify::selected-item", this._checkPreset.bind(this))
                            break;
                    }
                }
            }
        }
        // Connect menu signals
        this._builder.get_object('linkExtensionWebsite').connect(
            "clicked", () => this._openURI(window, this._url.repository)
        );
        this._builder.get_object('linkExtensionReferences').connect(
            "clicked", () => this._openURI(window, this._url.references)
        );
        this._builder.get_object('linkExtensionReportBug').connect(
            "clicked", () => this._openURI(window, this._url.bug_report)
        );

        if (this._shellVersion <= 41) {
            // Search function
            let searchButton = this._builder.get_object('searchButton');
            let searchEntry = this._builder.get_object('searchEntry');
            let searchBar = this._builder.get_object('searchBar');
            let searchPage = this._builder.get_object('searchPage');
            searchEntry.connect("changed", (w) => {
                this._searchConfig(w.get_text());
                if (this._searchWidgetResult.length == 0 && w.get_text() != "")
                    searchPage.set_visible_child_name("searchNotFound");
                else
                    searchPage.set_visible_child_name("searchAvailable");

            })

            let triggerSearch = () => {
                let mainStack = this._builder.get_object('mainStack');
                if (searchButton.get_active()) {
                    mainStack.set_visible_child_name("searchPage");
                } else {
                    mainStack.set_visible_child_name("contentPage");
                }
                this._searchConfig(searchEntry.get_text() || "");
            }
            searchButton.connect("toggled", triggerSearch);
            searchButton.bind_property(
                'active',
                searchBar,
                'search-mode-enabled',
                GObject.BindingFlags.BIDIRECTIONAL
            );
            if (Gtk.MAJOR_VERSION == 4) {
                searchBar.set_key_capture_widget(window);
            } else {
                window.connect('key-press-event', (win, event) => {
                    if (!isFocusWithinEntry) return searchBar.handle_event(event);
                });
            }

            searchBar.connect_entry(searchEntry);

            // search result item signal
            let configResult = this._builder.get_object("configResult")
            configResult.connect("row-activated", (list, listRow) => {
                let mainStack = this._builder.get_object('mainStack');
                mainStack.set_visible_child_name("contentPage");
                let contentStack = this._builder.get_object('contentStack');
                contentStack.set_visible_child_name("configPage");

                searchButton.set_active(false);
                searchBar.set_search_mode(false);

                // Focus to ListBoxRow
                listRow.get_child()._row.grab_focus();
            });
        }
    }

    _openURI(window, uri) {
        if (this._shellVersion >= 40)
            Gtk.show_uri(window, uri, Gdk.CURRENT_TIME);
        else
            Gtk.show_uri_on_window(window, uri, Gdk.CURRENT_TIME);
    }

    _checkState() {
        let panelVisibility = this._Keys.getKeys().panel.configs["panel-visibility"].widget;
        this._Keys.getKeys().panel.configs["panel-height"].widget
            .set_sensitive(panelVisibility.get_active());
        this._Keys.getKeys().panel.configs["panel-auto_hide"].widget
            .set_sensitive(panelVisibility.get_active());

        let panelAutoHide = this._Keys.getKeys().panel.configs["panel-auto_hide"].widget;
        this._Keys.getKeys().panel.configs["panel-visibility"].widget
            .set_sensitive(!panelAutoHide.get_active());
    }

    // _fixUI: Fixing ui elements
    _fixUI(window) {
        let window_size = [720, 480]
        window.default_width = window_size[0];
        window.default_height = window_size[1];
        if (Gtk.MAJOR_VERSION == 4) {
            // Add home cover image
            let coverImage = Gtk.Picture.new_for_filename(
                this._Self.dir.get_child('image').get_path() + "/gnome-shell.png"
            );
            coverImage.set_size_request(-1, 240)
            this._builder.get_object('coverImage').append(coverImage);

            // Add logo
            let logoImage = Gtk.Picture.new_for_filename(
                this._Self.dir.get_child('image').get_path() + "/sc-logo.png"
            );
            logoImage.set_size_request(-1, 96)
            this._builder.get_object('logoImage').append(logoImage);

            // Set scroll to focused widget for configPageView (Gtk.Viewport)
            let configPageView = this._builder.get_object('configPageView');
            configPageView.set_scroll_to_focus(true);
        } else {
            this._builder.get_object("headerBar").set_title('Shell Configurator');
            this._builder.get_object("headerBar").set_show_close_button(true);
            window.set_size_request(window_size[0], window_size[1]);
            window.resize(`${window_size[0]}`, `${window_size[1]}`);

            // Add home cover image
            let coverImage = Gtk.Image.new_from_file(
                this._Self.dir.get_child('image').get_path() + "/gnome-shell.png"
            );
            coverImage.set_visible(true);
            coverImage.set_pixel_size(240);
            this._builder.get_object('coverImage').pack_start(coverImage, false, false, 0);

            // Add logo
            let logoImage = Gtk.Image.new_from_file(
                this._Self.dir.get_child('image').get_path() + "/sc-logo.png"
            );
            logoImage.set_visible(true);
            this._builder.get_object('logoImage').pack_start(logoImage, false, false, 0);
        }
    }

    // _addPages: Add preferences pages
    _addPages(mainStack) {
        if (this._shellVersion >= 42) {
            const Adw = imports.gi.Adw;
            // Content page
            let welcomePage = new Adw.PreferencesPage();
            welcomePage.set_title('Home');
            welcomePage.set_name('welcomePage');
            welcomePage.set_icon_name('go-home-symbolic');
            let welcomeContainer = new Adw.PreferencesGroup({
                vexpand: true,
                valign: Gtk.Align.CENTER
            });
            welcomeContainer.add(this._builder.get_object('welcomePage'))
            welcomePage.add(welcomeContainer)
            mainStack.add(welcomePage);

            // Configuration Page
            let configPage = new Adw.PreferencesPage();
            configPage.set_title('Configurations');
            configPage.set_name('configPage');
            configPage.set_icon_name('emblem-system-symbolic');

            // Preset Section
            let presetGroup = new Adw.PreferencesGroup();
            let presetBox = new Gtk.Box({
                orientation: Gtk.Orientation.HORIZONTAL,
                halign: Gtk.Align.CENTER,
                spacing: 8,
                width_request: 560
            });
            let presetLabel = new Gtk.Label({
                label: 'Configuration Preset:',
            });
            this._presetCombo = new Gtk.ComboBoxText({
                hexpand: true
            });
            for (let [id, text] of Object.entries(this._presets)) {
                this._presetCombo.append(id, text);
            }
            this._presetCombo.append("custom", "Custom");
            this._presetCombo.connect('changed', () => {
                this._setPreset(this._presetCombo.get_active_id());
            })
            presetBox.append(presetLabel);
            presetBox.append(this._presetCombo);

            presetGroup.add(presetBox)
            configPage.add(presetGroup);

            // Module Pages
            for (let category of Object.values(this._Keys.getKeys())) {
                let container = new Adw.PreferencesGroup({
                    title: category.displayName,
                    description: category.description
                });

                for (let key of Object.values(category["configs"])) {
                    if (key.supported) {
                        let row, widget;
                        switch (key.type) {
                            case 'boolean':
                                widget = new Gtk.Switch({ valign: Gtk.Align.CENTER });
                                row = new Adw.ActionRow({
                                    name: key.id + '-state',
                                    title: key.display,
                                    subtitle: key.description,
                                    activatable_widget: widget
                                });
                                row.add_suffix(widget);
                                break;

                            case 'int':
                                let adjustment = new Gtk.Adjustment({
                                    lower: key.data[0],
                                    upper: key.data[1],
                                    step_increment: key.data[2]
                                });
                                widget = new Gtk.SpinButton({
                                    adjustment: adjustment,
                                    valign: Gtk.Align.CENTER
                                });

                                if (Gtk.MAJOR_VERSION == 4)
                                    widget.add_css_class('flat')
                                else
                                    widget.get_style_context().add_class('flat')

                                row = new Adw.ActionRow({
                                    name: key.id + '-size',
                                    title: key.display,
                                    subtitle: key.description,
                                    activatable_widget: widget
                                });
                                row.add_suffix(widget);
                                break;

                            case 'string':
                                widget = new Gtk.Entry({ valign: Gtk.Align.CENTER });
                                row = new Adw.ActionRow({
                                    name: key.id + '-value',
                                    title: key.display,
                                    subtitle: key.description,
                                    activatable_widget: widget
                                });
                                row.add_suffix(widget);
                                break;

                            case 'enum':
                                let stringList = new Gtk.StringList();
                                row = new Adw.ComboRow({
                                    name: key.id + '-item',
                                    title: key.display,
                                    subtitle: key.description,
                                    model: stringList
                                });
                                widget = row;
                                for (let item of key.data) {
                                    stringList.append(item[1]);
                                }
                                break;
                        }
                        container.add(row);
                        key["widget"] = widget;
                    }
                };

                configPage.add(container);
            }
            mainStack.add(configPage);

            // About page
            let aboutPage = new Adw.PreferencesPage();
            aboutPage.set_title('About');
            aboutPage.set_name('aboutPage');
            aboutPage.set_icon_name('dialog-information-symbolic');
            let aboutContainer = new Adw.PreferencesGroup({
                vexpand: true,
                valign: Gtk.Align.CENTER
            });
            aboutContainer.add(this._builder.get_object('aboutPage'))
            aboutPage.add(aboutContainer)
            mainStack.add(aboutPage);
        } else {
            // Content page
            let contentStack = this._builder.get_object('contentStack');
            mainStack.add_named(contentStack, "contentPage");

            // Welcome page
            let welcomePage = this._builder.get_object('welcomePage');
            contentStack.add_titled(welcomePage, "welcomePage", _("Home"));

            // Configuration Page
            let configPage = this._builder.get_object('configPage');
            contentStack.add_titled(configPage, "configPage", _("Configurations"));

            // Add presets entries
            this._presetCombo = this._builder.get_object('presetCombo');
            for (let [id, text] of Object.entries(this._presets)) {
                this._presetCombo.append(id, text);
            }
            this._presetCombo.append("custom", "Custom");
            this._presetCombo.connect('changed', () => {
                this._setPreset(this._presetCombo.get_active_id());
            })

            // Module Pages
            for (let category of Object.values(this._Keys.getKeys())) {
                let page = new this.modulePage(
                    category["configName"],
                    category["displayName"],
                    category["description"]
                );

                let index = 0
                let lastSep = null;
                for (let key of Object.values(category["configs"])) {
                    if (key.supported) {
                        let container = new this.configItem(key);
                        if (Gtk.MAJOR_VERSION == 4) {
                            let item = new Gtk.ListBoxRow({
                                activatable: false,
                                selectable: false
                            });
                            item.set_child(container);
                            page._configs.append(item);
                        } else {
                            page._configs.insert(container, index * 2);
                            // Separator
                            let sep = new Gtk.Separator({ visible: true });
                            page._configs.add(sep/* , index * 2 + 1 */);
                            lastSep = page._configs.get_row_at_index(index * 2 + 1);
                            lastSep.set_activatable(false);
                            index++;
                        }
                        key["widget"] = container._configControl;
                    }
                };
                // Remove last separator (for GTK+ 3 only)
                if (Gtk.MAJOR_VERSION == 3) page._configs.remove(lastSep);

                let modulePages = this._builder.get_object('modules');
                if (Gtk.MAJOR_VERSION == 4)
                    modulePages.append(page);
                else
                    modulePages.pack_start(page, false, true, 0);

                // Show list separator for GTK 4
                if (Gtk.MAJOR_VERSION == 4) page._configs.set_show_separators(true);
            }

            // About page
            let aboutPage = this._builder.get_object('aboutPage');
            contentStack.add_titled(aboutPage, "aboutPage", _("About"));

            // Search page
            let searchPage = this._builder.get_object('searchPage');
            searchPage.add_named(
                this._builder.get_object('searchAvailable'), "searchAvailable"
            );
            searchPage.add_named(
                this._builder.get_object('searchNotFound'), "searchNotFound"
            );
            mainStack.add_named(searchPage, "searchPage");
        }
    }

    // buildUI: Creates and build prefs UI and return to content widget which is use for
    //          buildPrefsWidget on prefs.js
    buildUI(window = null) {
        // Set translation domain
        this._builder.set_translation_domain("shell-configurator");

        // Adding preferences ui file
        let uiFiles = [ "window", "welcome-page", "config-page", "about-page", "search-page" ];
        for (let ui of uiFiles) {
            this._builder.add_from_file(
                `${this._Self.dir.get_child('ui').get_path()}/${ui}.ui`
            );
        }

        let windowContent; // for GNOME 41 below only

        if (this._shellVersion >= 42) {
            // Development window style
            if (this._isDevelopment(this._Self)) window.add_css_class('devel');

            // Fix the UI
            this._fixUI(window);

            // Enable search feature (built-in)
            window.search_enabled = true;

            // Reset all keys
            this._Keys.resetEmpty();
            // Add prefs keys
            this._addKeys();

            // Add preferences pages
            this._addPages(window);

            // Connect Signals
            this._connectSignals(window);

            // Check configuration state
            this._checkState();

            // Check preset
            this._checkPreset();
        } else {
            // Get content widget
            windowContent = this._builder.get_object('windowContent');

            // Connect content realize signal for window customization
            windowContent.connect('realize', () => {
                // Window configuration
                let window = (Gtk.MAJOR_VERSION == 4) ? windowContent.get_root() : windowContent.get_toplevel();

                // Header bar configuration for GNOME 41 below
                if (this._shellVersion <= 41) {
                    let stackSwitcher = this._builder.get_object('stackSwitcher');

                    let headerBar = this._builder.get_object("headerBar");

                    // Add title widget to header bar
                    if ((Gtk.MAJOR_VERSION == 4))
                        headerBar.set_title_widget(stackSwitcher);
                    else
                        headerBar.set_custom_title(stackSwitcher);

                    window.set_titlebar(headerBar);
                }

                // Development window style
                if (this._isDevelopment()) {
                    if (Gtk.MAJOR_VERSION == 4)
                        window.add_css_class('devel')
                    else
                        window.get_style_context().add_class('devel')
                }

                // Connect Signals
                this._connectSignals(window);

                // Check configuration state
                this._checkState();

                // Check preset
                this._checkPreset();

                // Fix the UI
                this._fixUI(window);
            });

            // Reset all keys
            this._Keys.resetEmpty();
            // Add prefs keys
            this._addKeys();
            // Add Pages
            let mainStack = this._builder.get_object('mainStack');
            this._addPages(mainStack);

            let contentStack = this._builder.get_object('contentStack');
            // Set stack for sidebar
            this._builder.get_object('stackSwitcher').set_stack(contentStack);
        }


        // Set GNOME Shell version label
        this._builder.get_object('shellVersionLabel').set_text(
            `${_("Current GNOME Shell Version")}: ` +
            ((this._shellVersion >= 40) ?
                this._shellVersion.toFixed(1)
            :   this._shellVersion.toFixed(2))
        );

        // Check if extension is in development, if true, make development message visible
        if (this._isDevelopment()) this._builder.get_object('developmentMessage').set_visible(true);

        // Set extension version label
        let versionText = `${_("Version")} ${this._Self.metadata['version']}`;
        versionText +=
            this._isDevelopment() ?
                ` - ${_("Codename")} ${this._Self.metadata['codename']}` + 
                `\n${_("Development Release")}`
            :   ""
        ;

        this._builder.get_object('versionLabel').set_text(versionText);

        // Returning content widget (for GNOME 41 below)
        if (this._shellVersion <= 41) return windowContent;
    }
}