/**
 * SCMisc
 * 
 * Description  Some additional functions and tools for Shell Configurator
 * Filename     misc.js
 * License      GNU General Public License v3.0
 */

const Self = imports.misc.extensionUtils.getCurrentExtension();

// Translations
const Gettext = imports.gettext;
const Domain = Gettext.domain(Self.metadata['gettext-domain']);
const _ = Domain.gettext;

var SCMisc = class {
    constructor(Main, currentPath, shellSettings) {
        this._Main = Main;
        this._currentPath = currentPath;
        this._shellSettings = shellSettings;

        this._shellVersion = parseFloat(imports.misc.config.PACKAGE_VERSION);
    }

    // isLocked: Check if session mode is in lock screen 'unlock-dialog'
    isLocked() {
        return this._Main.sessionMode.currentMode === 'unlock-dialog';
    }

    // isExtensionEnabled: Check if this extension is enabled.
    isExtensionEnabled() {
        return this._shellSettings.get_strv('enabled-extensions').includes(
            this._currentPath.metadata['uuid']
        );
    }

    // getSCStyle: get extension class name so that it can add the style name correctly
    getSCStyle(className, support) {
        if (support == 'all')
            return 'sc-' + className;
        else
            return 'sc-' + className + '-' + support.replace('.', '');
    }

    // byteArrayToString: Convert byte array to string
    byteArrayToString(byteArray) {
        if (byteArray) {
            if (this._shellVersion >= 41)
                return new TextDecoder('utf-8').decode(byteArray);
            else
                return imports.byteArray.toString(byteArray);
        }
    }

    // animate: animate/ease element compatible for all shell version
    animate(element, props) {
        if (this._shellVersion <= 3.36)
            imports.ui.tweener.addTween(element, props);
        else
            element.ease(props);
    }
}