const Self = imports.misc.extensionUtils.getCurrentExtension();
const {
    schema:Schema,
} = Self.imports.library;
const template = Self.imports.library.modules._template;

var Overview = class extends template.ConfigurationTemplate {
    constructor() {
        super();

        // ui libraries
        this._OverviewControls = imports.ui.overviewControls || null;
        this._WorkspaceThumbnail = imports.ui.workspaceThumbnail || null;
    }

    // backup: Backup shell properties
    backup() {
        super.backup();
        this._origin = {
            'workspaceSwitcherVisibility':  (this._shellVersion <= 3.38) ?
                                                this._OverviewControls.ThumbnailsSlider.prototype
                                                    ._getAlwaysZoomOut
                                            :   this._workspaceSwitcherScaleIsZero(),
            'workspaceSwitcherPeekWidth':   (this._shellVersion <= 3.38) ?
                                                this._OverviewControls.ThumbnailsSlider.prototype
                                                    .getNonExpandedWidth
                                            :   0,
            'workspaceSwitcherScaleSize':   (this._shellVersion >= 40) ?
                                                this._WorkspaceThumbnail.MAX_THUMBNAIL_SCALE
                                            :   5,
        }
    }

    // restore: Restore properties
    restore() {
        this.workspaceSwitcherVisibility(true);
        if (this._shellVersion <= 3.38) {
            this.workspaceSwitcherPeekWidth(-1);
        } else {
            this.workspaceSwitcherScaleSize(this._origin['workspaceSwitcherScaleSize'] * 100, false);
        }
    }

    // workspaceSwitcherVisibility: Workspace switcher visibility
    workspaceSwitcherVisibility(state, originSize) {
        if (state) {
            if (this._shellVersion >= 40) {
                // Set scale size to previous size
                this.workspaceSwitcherScaleSize(originSize, true);
                // Remove 'hidden-workspace-switcher' style
                this._Main.layoutManager.uiGroup.remove_style_class_name(
                    this._Misc.getSCStyle('hidden-workspace-switcher', '40')
                );
            } else {
                // Make thumbnail slider trigger again like previous.
                this._OverviewControls.ThumbnailsSlider.prototype._getAlwaysZoomOut =
                    this._origin['workspaceSwitcherVisibility'];
                // Set peek width to previous width
                this.workspaceSwitcherPeekWidth(originSize);
                // Remove 'hidden-workspace-switcher' style
                this._Main.layoutManager.uiGroup.remove_style_class_name(
                    this._Misc.getSCStyle('hidden-workspace-switcher', '3.38')
                );
            }
        } else {
            if (this._shellVersion >= 40) {
                // Set scale size to 0
                this.workspaceSwitcherScaleSize(0, true);
                // Add 'hidden-workspace-switcher' style to make workspace indicator not shown
                this._Main.layoutManager.uiGroup.add_style_class_name(
                    this._Misc.getSCStyle('hidden-workspace-switcher', '40')
                );
            } else {
                // Make thumbnail slider didn't trigger
                this._OverviewControls.ThumbnailsSlider.prototype._getAlwaysZoomOut = () => {
                    return false;
                };
                // Set peek width to 0
                this.workspaceSwitcherPeekWidth(0);
                // Add 'hidden-workspace-switcher' style
                this._Main.layoutManager.uiGroup.add_style_class_name(
                    this._Misc.getSCStyle('hidden-workspace-switcher', '3.38')
                );
            }
        }
    }

    // workspaceSwitcherPeekWidth: Workspace switcher peek width when it isn't in expanded state
    workspaceSwitcherPeekWidth(width) {
        if (this._shellVersion <= 3.38) {
            if (width == -1) {
                // Set peek width to previous width
                this._OverviewControls.ThumbnailsSlider.prototype.getNonExpandedWidth =
                    this._origin['workspaceSwitcherPeekWidth'];
            } else if (width >= 0 && width <= 96) {
                // Set peek width by returning user value
                this._OverviewControls.ThumbnailsSlider.prototype.getNonExpandedWidth = () => {
                    return width;
                }
            }
        }
    }

    // workspaceSwitcherScaleSize: Workspace switcher scale size on the top of overview on overview
    //                             state
    workspaceSwitcherScaleSize(percentage, unlimited) {
        if (this._shellVersion >= 40) {
            // It use for temporary
            if (unlimited) {
                this._WorkspaceThumbnail.MAX_THUMBNAIL_SCALE = percentage / 100;
                return;
            }
            if (percentage >= 2 && percentage <= 10) {
                // Set scale size by changing maximum scale size
                this._WorkspaceThumbnail.MAX_THUMBNAIL_SCALE = percentage / 100;
            }
        }
    }

    // _workspaceSwitcherScaleIsZero: Check if workspace switcher scale size is 0 (zero)
    _workspaceSwitcherScaleIsZero() {
        // Check by getting MAX_THUMBNAIL_SCALE variable from WorkspaceThumbnail library
        if (this._shellVersion >= 40) {
            if (this._WorkspaceThumbnail.MAX_THUMBNAIL_SCALE >= 0.02) {
                return true;
            } else {
                return false;
            }
        }
    }
}