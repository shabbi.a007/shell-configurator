const Self = imports.misc.extensionUtils.getCurrentExtension();
const {
    schema:Schema,
} = Self.imports.library;
const template = Self.imports.library.modules._template;

var Search = class extends template.ConfigurationTemplate {
    constructor() {
        super();

        // gi libraries
        this._Clutter = imports.gi.Clutter || null;

        // ui libraries
        this._searchController = (this._shellVersion >= 40) ? imports.ui.searchController : null;

        // Conditional declarations
        this._searchEntry =
            (this._shellVersion >= 3.36) ?
                this._Main.overview.searchEntry
            :   this._Main.overview._searchEntry;
        this._viewSelector =
            (this._shellVersion >= 3.36) ?
                this._Main.overview._overview.viewSelector
            :   this._Main.overview.viewSelector;

        // Private declarations
        this._visible = true;
    }

    // backup: Backup shell properties
    backup() {
        super.backup();
        this._origin = {
            'visibility':       this._searchEntry.visible,
            'typeToSearch':     (this._shellVersion >= 40 && this._searchController)
                                ?   this._searchController.SearchController.prototype.startSearch
                                :   this._viewSelector.startSearch,
            '_entryHeight':     this._searchEntry.height,
            '_parentHeight':    this._searchEntry.get_parent().height,
            '_height':          this._searchEntry.get_parent().height,
        }
    }

    // restore: Restore properties
    restore() {
        this.visibility(this._origin['visibility']);
        this.typeToSearch(String(this._origin['typeToSearch']) != '() => {}');
    }

    // visibility: Search entry visibility
    visibility(state) {
        let parent = this._searchEntry.get_parent();
        let commonAnimationProp = {
            transition: 'easeOutQuad',
            mode: this._Clutter.AnimationMode.EASE_OUT_QUAD,
            time: 100 / 1000,
            duration: 100,
        };

        let show = () => {
            this._Misc.animate(parent, {
                height: this._origin['_entryHeight'],
                opacity: 255,
                ... commonAnimationProp,
                onComplete: () => {
                    parent.height =
                        (this._shellVersion >= 3.36) ? -1 : this._origin['_parentHeight'];
                    this._Misc.animate(this._searchEntry, {
                        opacity: 255,
                        ... commonAnimationProp,
                    });
                }
            });
        }
        let hide = () => {
            this._Misc.animate(this._searchEntry, {
                opacity: 0,
                ... commonAnimationProp,
                onComplete: () => {
                    this._Misc.animate(parent, {
                        height: this._origin['_entryHeight'],
                        opacity: 255,
                        ... commonAnimationProp,
                    });
                }
            });
        }

        // Show search entry parent when state is true
        if (state) {
            parent.show()
            show();
        } else {
            hide();
        }

        if (!this.temporaryVisible) this._visible = state;
    }

    // typeToSearch: Type to search behavior
    typeToSearch(state) {
        //this.temporaryVisible = true;
        if (state) {
            if (this._shellVersion >= 40 && this._searchController)
                this._searchController.SearchController.prototype.startSearch = this._origin['typeToSearch'];
            else
                this._viewSelector.startSearch = this._origin['typeToSearch'];
        } else {
            if (this._shellVersion >= 40 && this._searchController)
                this._searchController.SearchController.prototype.startSearch = () => {};
            else
                this._viewSelector.startSearch = () => {};
        }
        this._startSearchHandler(state);
    }

    // _startSearchHandler: Connect start search signal when search entry is hidden
    _startSearchHandler(state) {
        let controller =
            (this._shellVersion >= 40)
            ?   this._Main.overview._overview.controls._searchController
            :   this._viewSelector;
        if (state) {
            let usesSearchController = this._shellVersion >= 40 && this._searchController;
            let signalName = (usesSearchController) ? 'notify::search-active' : 'page-changed';
            this._searchActiveSignal = controller.connect(signalName, () => {
                let inSearch =
                    (usesSearchController)
                    ?   controller.searchActive : controller._searchActive
                // Only affected when parent of search visibility is hidden
                this.temporaryVisible = inSearch;
                if (!this._visible) this.visibility(inSearch);
            })
        } else {
            controller.disconnect(this._searchActiveSignal);
            this._searchActiveSignal = null;
        }
    }
}