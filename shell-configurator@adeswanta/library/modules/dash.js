const Self = imports.misc.extensionUtils.getCurrentExtension();
const {
    schema:Schema,
} = Self.imports.library;
const template = Self.imports.library.modules._template;

var Dash = class extends template.ConfigurationTemplate {
    constructor() {
        super();

        // Conditional declarations
        this._dash =
            (this._shellVersion >= 3.36) ?
                this._Main.overview.dash
            :   this._Main.overview._dash.actor;
    }

    // backup: Backup shell properties
    backup() {
        super.backup();
        this._origin = {
            'visibility': this._dash.visible,
        }
    }

    // restore: Restore properties
    restore() {
        this.visibility(this._origin['visibility']);
    }

    // visibility: Dash visibility
    visibility(state) {
        if (!this._Misc.isLocked()) {
            if (state) {
                // Show the dash
                this._dash.show();
                // Set dash size to -1 (Default/fit)
                if (this._shellVersion >= 40) {
                    this._Main.overview.dash.height = -1;
                    this._Main.overview.dash.setMaxSize(-1, -1);
                } else if (this._shellVersion >= 3.36) {
                    this._Main.overview.dash.width = -1;
                    this._Main.overview.dash._maxHeight = -1;
                }
            } else {
                // Hide the dash
                this._dash.hide();
                // Set dash size to 0 (not shown) to make view selector/workspace move closer to
                // bottom/left
                if (this._shellVersion >= 40) {
                    this._Main.overview.dash.height = 0;
                    this._Main.overview.dash.setMaxSize(0, 0);
                } else if (this._shellVersion >= 3.36) {
                    this._Main.overview.dash.width = 0;
                    this._Main.overview.dash._maxHeight = 0;
                }
            }
        }
    }
}