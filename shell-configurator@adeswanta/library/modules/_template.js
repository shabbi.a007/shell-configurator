/**
 * This is a template of modules. DO NOT MODIFY THIS FILE.
 */

const Self = imports.misc.extensionUtils.getCurrentExtension();
const {
    schema:SCSchema,
    misc:Misc,
} = Self.imports.library;

var ConfigurationTemplate = class {
    constructor() {
        // UI Main
        this._Main = imports.ui.main;

        // SC libraries
        this._Misc = new Misc.SCMisc(this._Main, Self, SCSchema.newSchema('org.gnome.shell'));

        // Private declarations
        this._shellVersion = parseFloat(imports.misc.config.PACKAGE_VERSION);
    }

    // backup: Backup shell properties
    backup() {
        if (this._origin) { this._origin = {}; }
    }

    // restore: Restore properties
    restore() { }
}