const Self = imports.misc.extensionUtils.getCurrentExtension();
const {
    schema:Schema,
} = Self.imports.library;
const template = Self.imports.library.modules._template;

var LookingGlass = class extends template.ConfigurationTemplate {
    constructor() {
        super();

        // Initialize/create looking glass in order not to make looking glass isn't null
        this._Main.createLookingGlass();

        // Conditional declarations
        if (this._shellVersion >= 3.36) {
            this._panel = this._Main.panel;
            this._lookingGlass = this._Main.lookingGlass;
        } else {
            this._panel = this._Main.panel.actor;
            this._lookingGlass = this._Main.lookingGlass.actor;
        }
    }

    // backup: Backup shell properties
    backup() {
        super.backup();
        this._origin = {
            'horizontal-position':
                this._lookingGlass.x / (
                    this._Main.layoutManager.primaryMonitor.width - this._lookingGlass.width
                ),
            'vertical-position': this._getPosition(),
            'width': this._lookingGlass.width,
            'height': this._lookingGlass.height
        }
    }

    // restore: Restore properties
    restore() {
        this.horizontalPosition(this._origin['horizontal-position']);
        this.verticalPosition(this._origin['vertical-position']);
        this.height(this._origin['height']);
        this.width(this._origin['width']);

        // Resize (fully) to original
        this._Main.lookingGlass._resize();
    }

    // horizontalPosition: Looking Glass horizontal position (in percentage)
    horizontalPosition(size) {
        this._lookingGlass.x =
            this._Main.layoutManager.primaryMonitor.x + (
                this._Main.layoutManager.primaryMonitor.width -  this._lookingGlass.width
            ) * (size / 100)
    }

    // verticalPosition: Looking Glass vertical position
    verticalPosition(position) {
        let pos = 0
        switch (position) {
            case 0: // TOP
                pos =
                    this._Main.layoutManager.primaryMonitor.y + (
                        (this._panel.visible && this._Main.layoutManager.panelBox.y == 0) ?
                            this._panel.height : 0
                    )
                this._Main.lookingGlass._hiddenY = pos - this._lookingGlass.height;
                this._Main.lookingGlass._targetY = pos;
                // Make looking glass keep shown after inspecting shell elements
                if (!this._Main.lookingGlass._open)
                    this._lookingGlass.y = this._Main.lookingGlass._hiddenY;
                else
                    this._lookingGlass.y = pos;
                break;
            
            case 1: // BOTTOM
                pos = 
                    this._Main.layoutManager.primaryMonitor.y + (
                        this._Main.layoutManager.primaryMonitor.height -
                        this._lookingGlass.height - (
                            (
                                // Check if panel position is on the bottom
                                this._panel.visible && this._Main.layoutManager.panelBox.y == (
                                    this._Main.layoutManager.primaryMonitor.height -
                                    this._panel.height
                                )
                            ) ? this._panel.height : 0
                        )
                    );
                    this._Main.lookingGlass._hiddenY = 
                    this._Main.layoutManager.primaryMonitor.height - (
                        (
                            // Check if panel position is on the bottom
                            this._panel.visible && this._Main.layoutManager.panelBox.y == (
                                this._Main.layoutManager.primaryMonitor.height -
                                this._panel.height
                            )
                        ) ? this._panel.height : 0
                    );
                this._Main.lookingGlass._targetY = pos;
                // Make looking glass keep shown after inspecting shell elements
                if (!this._Main.lookingGlass._open)
                    this._lookingGlass.y = this._Main.lookingGlass._hiddenY;
                else
                    this._lookingGlass.y = pos;
                break;
        }
    }

    // width: Looking Glass width size
    width(size) {
        let position = (
            this._lookingGlass.x / (
                this._Main.layoutManager.primaryMonitor.width - this._lookingGlass.width
            )
        ) * 100
        this._lookingGlass.width = size;
        // Repositioning
        this.horizontalPosition(position);
    }

    // height: Looking Glass height size
    height(size) {
        this._lookingGlass.height = size;
        // Repositioning
        this.verticalPosition(this._getPosition())
    }

    // _getPosition: Get Looking Glass position
    _getPosition() {
        // Check panel y position
        let verticalPosition = this._Main.lookingGlass._targetY;
        let targetPosition = this._Main.layoutManager.primaryMonitor.y + this._panel.height;
        // return 0 for TOP and 1 for BOTTOM
        return (verticalPosition <= targetPosition) ? 0 : 1;
    }
}