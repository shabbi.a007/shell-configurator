const Self = imports.misc.extensionUtils.getCurrentExtension();
const {
    schema:Schema,
} = Self.imports.library;
const template = Self.imports.library.modules._template;

const PANEL_POSITION = {
    TOP: 0,
    BOTTOM: 1,
};

var Panel = class extends template.ConfigurationTemplate {
    constructor() {
        super();

        // gi libraries
        this._Clutter = imports.gi.Clutter;

        // Schemas
        this._desktopInterfaceSchema = Schema.newSchema('org.gnome.desktop.interface');

        // Conditional declarations
        if (this._shellVersion >= 3.36)
            this._panel = this._Main.panel;
        else
            this._panel = this._Main.panel.actor;

        // Private declarations
        this._autoHide = false;
        this._panelDirection = (this._getPosition() == PANEL_POSITION.TOP) ? -1 : 1;
        this._animationDuration = 240;
    }

    // backup: Backup shell properties
    backup() {
        super.backup();
        this._origin = {
            'visibility':       this._panel.visible,
            'autoHide':         this._autoHide,
            'height':           this._panel.height,
            'panelPosition':    this._getPosition(),
            'hotCorner':        (this._shellVersion >= 3.34) ?
                                    this._desktopInterfaceSchema.get_boolean('enable-hot-corners')
                                :   true
        }
    }

    // restore: Restore properties
    restore() {
        this.visibility(this._origin['visibility']);
        this.autoHide(this._origin['autoHide']);
        this.height(this._origin['height']);
        this.position(this._origin['panelPosition']);
        this.hotCorner(this._origin['hotCorner']);
    }

    _animateShow() {
        // Animation Properties
        if (this._autoHide) this._panel.show();
        let animationProp = {
            translation_y: 0,
            transition: 'easeOutQuad',
            mode: this._Clutter.AnimationMode.EASE_OUT_QUAD,
            time: this._animationDuration / 1000,
            duration: this._animationDuration,
            onComplete: () => {
                // Restores trackedActor properties for panelBox
                // But don't do this when panel is in auto hide mode
                if (!this._autoHide) {
                    let trackedIndex =
                        this._Main.layoutManager
                            ._findActor(this._Main.layoutManager.panelBox);
                    this._Main.layoutManager._trackedActors[trackedIndex].affectsStruts = true;
                }

                // hide and show can fix windows not going under panel
                this._Main.layoutManager.panelBox.hide();
                this._Main.layoutManager.panelBox.show();
            }
        }

        // Run Animation
        this._Misc.animate(this._Main.layoutManager.panelBox, animationProp);
    }

    _animateHide() {
        let panelHeight = this._panel.height;
        // Animation Properties
        let animationProp = {
            translation_y: panelHeight * this._panelDirection,
            transition: 'easeOutQuad',
            mode: this._Clutter.AnimationMode.EASE_OUT_QUAD,
            time: this._animationDuration / 1000,
            duration: this._animationDuration,
            onComplete: () => {
                // Hide the panel
                this._panel.hide();
            },
        }

        // Run Animation
        this._Misc.animate(this._Main.layoutManager.panelBox, animationProp);
    }

    // visibility: Panel visibility
    visibility(state) {
        let show = () => {
            // Show the panel ghost
            if (this._shellVersion <= 3.38) this._Main.overview._overview.get_children()[0].show();

            // Show the panel
            this._panel.show();
            
            // Remove 'fix-338 / fix-40' style
            if (this._shellVersion >= 40)
                this._Main.layoutManager.uiGroup.remove_style_class_name('fix-40');
            else
                this._Main.layoutManager.uiGroup.remove_style_class_name('fix-338');
            
            // Remove 'no-top-panel' style
            this._Main.layoutManager.uiGroup.remove_style_class_name(
                this._Misc.getSCStyle('no-top-panel', 'all')
            );

            this._animateShow();
        }
        let hide = () => {
            // Hide the panel ghost
            if (this._shellVersion <= 3.38) this._Main.overview._overview.get_children()[0].hide();

            // When auto hide:
            // make panel overlap by changing trackedActor properties for panelBox
            let trackedIndex =
                this._Main.layoutManager
                    ._findActor(this._Main.layoutManager.panelBox);
            this._Main.layoutManager._trackedActors[trackedIndex].affectsStruts = false;

            // Add 'fix-338 / fix-40' style to make search entry not closer to top
            if (this._shellVersion >= 40)
                this._Main.layoutManager.uiGroup.add_style_class_name('fix-40');
            else
                this._Main.layoutManager.uiGroup.add_style_class_name('fix-338');
            
            // Add 'no-top-panel' style
            this._Main.layoutManager.uiGroup.add_style_class_name(
                this._Misc.getSCStyle('no-top-panel', 'all')
            );

            this._animateHide();
        }
        
        if (state) {
            show();
        } else {
            if (!this._Misc.isLocked())
                hide();
            else
                show();
        }
    }

    _checkPanelEnter() {
        if (this._getPosition() == PANEL_POSITION.TOP) {
            return global.get_pointer()[1] >= this._targetLeave;
        } else {
            return global.get_pointer()[1] <= this._targetLeave;
        }
    }

    _handleBlockedMenu() {
        this._blockedMenu = this._Main.panel.menuManager.activeMenu;
        if (this._blockedMenu == null) {
            if (this._checkPanelEnter()) this._animateHide();
        } else {
            this._blockedMenuId = this._blockedMenu.connect('open-state-changed', (menu, open) => {
                if (!open && this._blockedMenu !== null && this._autoHide) {
                    this._blockedMenu.disconnect(this._blockedMenuId);
                    this._blockedMenuId = null;
                    this._blockedMenu = null;
                    this._handleBlockedMenu();
                }
            });
        }
    }

    autoHide(state, update = false) {
        if (!this._Misc.isLocked()) {
            let primaryMonitor = this._Main.layoutManager.primaryMonitor;
            if (state) {
                // Add pressure barrier
                this._pressureBarrier = new imports.ui.layout.PressureBarrier(
                    50,
                    1000,
                    imports.gi.Shell.ActionMode.NORMAL |
                    imports.gi.Shell.ActionMode.OVERVIEW
                );
                let yPos = (this._getPosition() == PANEL_POSITION.TOP) ?
                    0
                :   primaryMonitor.height;
                this._barrier = new imports.gi.Meta.Barrier({
                    display: global.display,
                    x1: primaryMonitor.x,
                    x2: primaryMonitor.x + primaryMonitor.width,
                    y1: yPos,
                    y2: yPos,
                    directions: (this._getPosition() == PANEL_POSITION.TOP) ?
                        imports.gi.Meta.BarrierDirection.POSITIVE_Y
                    :   imports.gi.Meta.BarrierDirection.NEGATIVE_Y
                });
                this._pressureBarrier.addBarrier(this._barrier);

                this._targetLeave = (this._getPosition() == PANEL_POSITION.TOP) ?
                    this._panel.height
                :   primaryMonitor.height - this._panel.height;

                // Set track_hover and reactive state for panelBox
                this._Main.layoutManager.panelBox.set({ track_hover: true, reactive: true });

                // Add hover callbacks
                this._hoverCallbacks = [];
                this._hoverCallbacks.push(
                    this._pressureBarrier.connect('trigger', () => {
                        if (this._autoHide) this._animateShow();
                    })
                );
                this._hoverCallbacks.push(
                    this._Main.layoutManager.panelBox.connect('leave-event', () => {
                        if (this._checkPanelEnter() && this._autoHide &&
                            (this._Main.panel.menuManager.activeMenu == null) ) {
                            this._animateHide();
                        }
                        if (this._Main.panel.menuManager.activeMenu != null && this._autoHide) {
                            this._handleBlockedMenu();
                        }
                    })
                );
                
                if (this._isVisible && !update) this.visibility(false);
                this._autoHide = state;
            } else {
                if (this._pressureBarrier) {
                    this._autoHide = state;
                    if (!update) this.visibility(true);
                    this._pressureBarrier.disconnect(this._hoverCallbacks[0]);
                    this._Main.layoutManager.panelBox.disconnect(this._hoverCallbacks[1]);
                    this._targetLeave = null;
                    this._pressureBarrier.removeBarrier(this._barrier);
                    this._pressureBarrier = null;

                    // Revert track_hover and reactive state for panelBox
                    this._Main.layoutManager.panelBox.set({ track_hover: false, reactive: false });

                    if (!update) {
                        this._barrier.destroy();
                        this._barrier = null;
                    }
                }
            }
            // Reposition panel translation
            if (this._autoHide && update)
                this._Main.layoutManager.panelBox.translation_y =
                    this._panel.height * this._panelDirection;
        }
    }

    // height: Panel height size
    height(size, unlimited) {
        if (!this._Misc.isLocked()) {
            // It use for temporary
            if (unlimited) {
                this._panel.height = size;
                return;
            }
            if (size >= 16 && size <= 128) {
                // Make panel height changes if panel is visible
                if (this._isVisible()) {
                    // Set panel height
                    this._panel.height = size;
                }
            }
            // Repositioning
            this.position(this._getPosition());
        }
    }

    // position: Panel position
    position(position) {
        if (!this._Misc.isLocked()) {
            switch (position) {
                case PANEL_POSITION.TOP:
                    // Set position to top
                    this._Main.layoutManager.panelBox.set_position(0, 0);
                    
                    // Check if panel is visible. If true, remove style classes
                    if (this._isVisible()) {
                        // Remove 'no-top-panel' style
                        this._Main.layoutManager.uiGroup.remove_style_class_name(
                            this._Misc.getSCStyle('no-top-panel', 'all')
                        );
                        // Remove 'fix-338 / fix-40' style
                        if (this._shellVersion >= 40) {
                            this._Main.layoutManager.uiGroup.remove_style_class_name('fix-40');
                        } else {
                            this._Main.layoutManager.uiGroup.remove_style_class_name('fix-338');
                        }
                        // Show the panel ghost
                        if (this._shellVersion <= 3.38) {
                            this._Main.overview._overview.get_children()[0].show();
                        }
                    }
                    // Reset the overview control style
                    if (this._shellVersion >= 3.36)
                        this._Main.overview._overview._controls.style = null;
                    else
                        this._Main.overview._controls.actor.style = null;
                    
                    // Remove 'bottom-panel' style
                    this._Main.layoutManager.uiGroup.remove_style_class_name('bottom-panel');
                    
                    // Change panelDirection
                    this._panelDirection = -1;
                    break;
                
                case PANEL_POSITION.BOTTOM: // BOTTOM
                    // Declare primary monitor resolution size
                    let primaryMonitor = this._Main.layoutManager.primaryMonitor.height;
                    let panelHeight = this._panel.height;
                    // Set position to bottom
                    this._Main.layoutManager.panelBox.set_position(0, primaryMonitor - panelHeight);
                    
                    // Add 'no-top-panel' style
                    this._Main.layoutManager.uiGroup.add_style_class_name(
                        this._Misc.getSCStyle('no-top-panel', 'all')
                    );
                    // Add 'bottom-panel' style to make panel menu move closer to bottom panel
                    this._Main.layoutManager.uiGroup.add_style_class_name('bottom-panel');
                    // Add 'fix-338 / fix-40' style to make search entry not closer to top
                    if (this._shellVersion >= 40) {
                        this._Main.layoutManager.uiGroup.add_style_class_name('fix-40');
                    } else {
                        this._Main.layoutManager.uiGroup.add_style_class_name('fix-338');
                    }
                    
                    // Hide the panel ghost
                    if (this._shellVersion <= 3.38) {
                        this._Main.overview._overview.get_children()[0].hide();
                    }
                    
                    // Set overview controls style to make panel didn't overlap the overview/window
                    // previews
                    if (this._shellVersion >= 3.36) {
                        this._Main.overview._overview._controls.style =
                            "margin-bottom: " + this._Main.panel.height + "px;";
                    }
                    else {
                        this._Main.overview._controls.actor.style =
                            "margin-bottom: " + this._Main.panel.actor.height + "px;";
                    }
                    
                    // Change panelDirection
                    this._panelDirection = 1;
                    break;
            }
            // Reposition panel translation when panel is invisible
            if (!this._isVisible())
                this._Main.layoutManager.panelBox.translation_y =
                    this._panel.height * this._panelDirection;
        }
    }

    // hotCorner: Top left trigger (hot corner) visibility
    hotCorner(state) {
        if (this._shellVersion >= 3.34) {
            this._desktopInterfaceSchema.set_boolean('enable-hot-corners', state)
        } else {
            if (state) {
                this._Main.layoutManager._updateHotCorners();
            } else {
                this._Main.layoutManager.hotCorners.forEach( (hot_corner) => {
                    if (!hot_corner) return;
                
                    hot_corner._toggleOverview = function() {};
                    hot_corner._pressureBarrier._trigger = function() {};
                });
                this._desktopInterfaceSchema.connect("changed::enable-hot-corners", () => {
                    this.hotCorner(
                        this._desktopInterfaceSchema.get_boolean('enable-hot-corners')
                    );
                })
            }
        }
    }

    // _getPosition: Get panel position
    _getPosition() {
        // Check panel y position
        let panelYPosition = this._Main.layoutManager.panelBox.get_position()[1];
        return (panelYPosition !== 0) ? PANEL_POSITION.BOTTOM : PANEL_POSITION.TOP;
    }

    _isVisible() {
        // Return the panel visibility state
        return this._panel.visible;
    }
}