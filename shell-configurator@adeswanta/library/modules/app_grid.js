const Self = imports.misc.extensionUtils.getCurrentExtension();
const {
    schema:Schema,
} = Self.imports.library;
const template = Self.imports.library.modules._template;

var AppGrid = class extends template.ConfigurationTemplate {
    constructor() { super(); }

    // backup: Backup shell properties
    backup() {
        super.backup();
        this._origin = {
            'rows':     (this._shellVersion >= 3.38) ?
                            (this._shellVersion >= 40) ?
                                this._Main.overview._overview._controls._appDisplay
                                    ._grid.layout_manager.rows_per_page 
                            :   this._Main.overview.viewSelector.appDisplay._grid.layout_manager
                                    .rows_per_page
                        :   this._Main.overview.viewSelector.appDisplay._views[0].view._grid
                                ._minRows
                        ||  this._Main.overview.viewSelector.appDisplay._views[1].view._grid
                                ._minRows,
            'columns':  (this._shellVersion >= 3.38) ?
                            (this._shellVersion >= 40) ?
                                this._Main.overview._overview._controls._appDisplay._grid
                                    .layout_manager.columns_per_page
                            :   this._Main.overview.viewSelector.appDisplay._grid.layout_manager
                                    .columns_per_page
                        :   this._Main.overview.viewSelector.appDisplay._views[0].view._grid
                                ._colLimit
                        ||  this._Main.overview.viewSelector.appDisplay._views[1].view._grid
                                ._colLimit,
            'gridModes': (this._shellVersion >= 40) ?
                            this._Main.overview._overview._controls._appDisplay._grid._gridModes
                        :   null
        }
        // Fixed from https://gitlab.com/adeswantaTechs/shell-configurator/-/issues/3
        // Make gridModes empty in order to make grid size persistent
        if (this._shellVersion >= 40)
            this._Main.overview._overview._controls._appDisplay._grid._gridModes = [];
    }

    // restore: Restore properties
    restore() {
        this.rows(this._origin['rows']);
        this.columns(this._origin['columns']);
        // Restore gridModes
        if (this._shellVersion >= 40) {
            this._Main.overview._overview._controls._appDisplay._grid._gridModes =
                this._origin['gridModes'];
        }
    }

    // rows: AppGrid row page item
    rows(size) {
        if (size >= 2 && size <= 12) {
            if (this._shellVersion >= 3.38) {
                if (this._shellVersion >= 40) {
                    // Set row size
                    this._Main.overview._overview._controls._appDisplay._grid.layout_manager
                        .rows_per_page = size;
                    // Redisplay app grid
                    this._Main.overview._overview._controls._appDisplay._redisplay();
                } else {
                    // Set row size
                    this._Main.overview.viewSelector.appDisplay._grid.layout_manager
                        .rows_per_page = size;
                    // Redisplay app grid
                    this._Main.overview.viewSelector.appDisplay._redisplay();
                }
            } else {
                // Set row size for frequent page
                this._Main.overview.viewSelector.appDisplay._views[0].view._grid._minRows = size;
                // Redisplay app grid
                this._Main.overview.viewSelector.appDisplay._views[0].view._redisplay();
                // Set row size for all (apps) page
                this._Main.overview.viewSelector.appDisplay._views[1].view._grid._minRows = size;
                // Redisplay app grid
                this._Main.overview.viewSelector.appDisplay._views[1].view._redisplay();
            }
        }
    }

    // columns: AppGrid column page item
    columns(size) {
        if (size >= 2 && size <= 12) {
            if (this._shellVersion >= 3.38) {
                if (this._shellVersion >= 40) {
                    // Set row size
                    this._Main.overview._overview._controls._appDisplay._grid.layout_manager
                        .columns_per_page = size;
                    // Redisplay app grid
                    this._Main.overview._overview._controls._appDisplay._redisplay();
                } else {
                    // Set column size
                    this._Main.overview.viewSelector.appDisplay._grid.layout_manager
                        .columns_per_page = size;
                    // Redisplay app grid
                    this._Main.overview.viewSelector.appDisplay._redisplay();
                }
            } else {
                // Set column size for frequent page
                this._Main.overview.viewSelector.appDisplay._views[0].view._grid._colLimit = size;
                // Redisplay app grid
                this._Main.overview.viewSelector.appDisplay._views[0].view._redisplay();
                // Set column size for all (apps) page
                this._Main.overview.viewSelector.appDisplay._views[1].view._grid._colLimit = size;
                // Redisplay app grid
                this._Main.overview.viewSelector.appDisplay._views[1].view._redisplay();
            }
        }
    }
}