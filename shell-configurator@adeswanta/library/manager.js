/**
 * Components manager for Shell Configurator Settings
 * 
 * @author      Advendra Deswanta <adeswanta@gmail.com>
 * @file        manager.js
 * @license     GNU General Public License v3.0
 */

var SCManager = class {
    constructor(Self, Main, Settings, Libs, shellVersion) {
        this._Self = Self;

        // Shell Main
        this._Main = Main;

        // Declare GNOME Shell Version
        this._shellVersion = shellVersion;

        // Declare GSettings schema
        this._shellSettings = Settings['shellSettings'] || null;
        this._extensionSettings = Settings['extensionSettings'] || null;

        // Declare Libraries
        this._Misc = Libs['Misc'];
        this._Components = Libs['Components'];

        // imports required library and components
        this._panel = this._Components.Panel;
        this._dash = this._Components.Dash;
        this._overview = this._Components.Overview;
        this._search = this._Components.Search;
        this._appGrid = this._Components.AppGrid;
        this._lookingGlass = this._Components.LookingGlass;

        // Saves the current shell properties before the extension is start modifying them
        this._Components.backup();

        // Connect some signals and callbacks
        this._connectSignals();
        this._connectCallbacks();

        this._applyChanges();
    }

    destroy() {
        // Disconnect connected signals and callbacks
        this._disconnectSignals();
        this._disconnectCallbacks();

        // Set to previous shell properties when extension is disabled
        this._Components.restore();

        // Makes variables are null (in order not to make increasing memory)

        this._panel = null;
        this._dash = null;
        this._overview = null;
        this._appGrid = null;
        this._lookingGlass = null;

        this._Components = null;
        this._Misc = null;

        this._shellSettings = null;
        this._extensionSettings = null;
        
        this._shellVersion = null;
        
        this._Main = null;

        this._Self = null;
    }

    // applyChanges: Sets all shell properties from shell settings
    _applyChanges() {
        // Panel
        this._panel.visibility(this._extensionSettings.get_boolean('panel-visibility'));
        this._panel.autoHide(this._extensionSettings.get_boolean('panel-auto-hide'));
        this._panel.height(this._extensionSettings.get_int('panel-height'));
        this._panel.position(this._extensionSettings.get_enum('panel-position'));
        this._panel.hotCorner(this._extensionSettings.get_boolean('panel-hot-corner'));
        // Dash
        this._dash.visibility(this._extensionSettings.get_boolean('dash-visibility'));
        // Overview
        this._overview.workspaceSwitcherVisibility(
            this._extensionSettings.get_boolean('overview-workspace-switcher-visibility')
        );
        if (this._shellVersion >= 40) {
            this._overview.workspaceSwitcherScaleSize(
                this._extensionSettings.get_int('overview-workspace-switcher-scale-size'),
                false
            );
        } else {
            this._overview.workspaceSwitcherPeekWidth(
                this._extensionSettings.get_int('overview-workspace-switcher-peak-width')
            );
        }
        // Search
        this._search.visibility(this._extensionSettings.get_boolean('search-visibility'));
        this._search.typeToSearch(this._extensionSettings.get_boolean('search-type-to-search'));
        // App Grid
        this._appGrid.rows(this._extensionSettings.get_int('appgrid-rows'));
        this._appGrid.columns(this._extensionSettings.get_int('appgrid-columns'));
        // Looking Glass
        // Initiate the changes will revert back using Main.lookingGlass._resize()
    }

    // connectSignals: Connects all extension settings signals when user change the settings
    _connectSignals() {
        this._signals = [
            // Panel
            [
                this._extensionSettings.connect('changed::panel-visibility', () => {
                    this._panel.visibility(this._extensionSettings.get_boolean('panel-visibility'));
                }),
                this._extensionSettings.connect('changed::panel-auto-hide', () => {
                    this._panel.autoHide(this._extensionSettings.get_boolean('panel-auto-hide'));
                }),
                this._extensionSettings.connect('changed::panel-height', () => {
                    this._panel.height(this._extensionSettings.get_int('panel-height'), false);
                }),
                this._extensionSettings.connect('changed::panel-position', () => {
                    this._panel.position(this._extensionSettings.get_enum('panel-position'));
                    if (this._panel._autoHide) {
                        this._panel.autoHide(false, true);
                        this._panel.autoHide(this._extensionSettings.get_boolean('panel-auto-hide'), true);
                    }
                }),
                this._extensionSettings.connect('changed::panel-hot-corner', () => {
                    this._panel.hotCorner(this._extensionSettings.get_boolean('panel-hot-corner'))
                })
            ],
            // Dash
            [
                this._extensionSettings.connect('changed::dash-visibility', () => {
                    this._dash.visibility(this._extensionSettings.get_boolean('dash-visibility'));
                })
            ],
            // Overview
            [
                this._extensionSettings.connect('changed::overview-workspace-switcher-visibility', () => {
                    this._overview.workspaceSwitcherVisibility(
                        this._extensionSettings.get_boolean('overview-workspace-switcher-visibility'),
                        (this._shellVersion >= 40) ?
                            this._extensionSettings.get_int('overview-workspace-switcher-scale-size')
                        :   this._extensionSettings.get_int('overview-workspace-switcher-peak-width')
                    );
                }),
                this._extensionSettings.connect('changed::overview-workspace-switcher-peak-width', () => {
                    if (this._shellVersion <= 3.38 &&
                        this._Misc.isExtensionEnabled(
                            this._Self, this._shellSettings) &&
                            this._extensionSettings.get_boolean('overview-workspace-switcher-visibility')
                    )
                        this._overview.workspaceSwitcherPeekWidth(
                            this._extensionSettings.get_int('overview-workspace-switcher-peak-width')
                        );
                }),
                this._extensionSettings.connect('changed::overview-workspace-switcher-scale-size', () => {
                    if (this._shellVersion >= 40 &&
                        this._Misc.isExtensionEnabled(this._Self, this._shellSettings) &&
                        this._extensionSettings.get_boolean('overview-workspace-switcher-visibility')
                    )
                        this._overview.workspaceSwitcherScaleSize(
                            this._extensionSettings.get_int('overview-workspace-switcher-scale-size')
                        );
                }),
                this._extensionSettings.connect('changed::overview-search-entry-visibility', () => {
                    this._overview.searchEntryVisibility(
                        this._extensionSettings.get_boolean('overview-search-entry-visibility')
                    );
                })
            ],
            // Search
            [
                this._extensionSettings.connect('changed::search-visibility', () => {
                    this._search.visibility(this._extensionSettings.get_boolean('search-visibility'));
                }),
                this._extensionSettings.connect('changed::search-type-to-search', () => {
                    this._search.typeToSearch(this._extensionSettings.get_boolean('search-type-to-search'));
                })
            ],
            // App Grid
            [
                this._extensionSettings.connect('changed::appgrid-rows', () => {
                    this._appGrid.rows(this._extensionSettings.get_int('appgrid-rows'));
                }),
                this._extensionSettings.connect('changed::appgrid-columns', () => {
                    this._appGrid.columns(this._extensionSettings.get_int('appgrid-columns'));
                })
            ],
            // Looking Glass
            // Note: It will changed when popup is shown
        ];
    }

    // disconnectSignals: Disconnect all extension settings signals that prevent from user changes
    //                    the settings
    _disconnectSignals() {
        for (let category of this._signals) {
            for (let signal of category) {
                this._extensionSettings.disconnect(signal);
            }
        }
        this._signals = []
    }

    // connectCallbacks: Connects all extension callbacks when other shell settings changed
    _connectCallbacks() {
        let panel, lookingGlass;
        if (this._shellVersion >= 3.36) {
            panel = this._Main.panel;
            lookingGlass = this._Main.lookingGlass;
        } else {
            panel = this._Main.panel.actor;
            lookingGlass = this._Main.lookingGlass.actor;
        }
        this._callbacks = [
            this._Main.layoutManager.connect("monitors-changed", () => {
                this._extensionSettings.set_int('panel-height', panel.height);
                this._panel.position(this._extensionSettings.get_enum('panel-position'));
            }),
            // For 3.32 below, hot corners may be re-created afterwards
            // (for example, If there's a monitor change)
            this._Main.layoutManager.connect('hot-corners-changed', () => {
                // It's only affected when hot-corner is disabled
                if (!this._extensionSettings.get_boolean('panel-hot-corner')) {
                    if (this._shellVersion <= 3.32)
                        this._panel.hotCorner(this._extensionSettings.get_boolean('panel-hot-corner'))
                }
            }),
            lookingGlass.connect('show', () => {
                // Change the looking glass configuration
                this._lookingGlass.horizontalPosition(
                    this._extensionSettings.get_int('lookingglass-horizontal-position')
                );
                this._lookingGlass.verticalPosition(
                    this._extensionSettings.get_enum('lookingglass-vertical-position')
                );
                this._lookingGlass.width(this._extensionSettings.get_int('lookingglass-width'));
                this._lookingGlass.height(this._extensionSettings.get_int('lookingglass-height'));
            }),
            lookingGlass.connect('hide', () => {
                // Make looking glass keep shown after inspecting shell elements
                this._lookingGlass.verticalPosition(
                    this._extensionSettings.get_enum('lookingglass-vertical-position')
                );
            })
        ]
    }

    // disconnectCallbacks: Disconnect all extension callbacks that prevent from shell changes the
    //                      properties
    _disconnectCallbacks() {
        let lookingGlass =
            (this._shellVersion >= 3.36) ?
                this._Main.lookingGlass
            :   this._Main.lookingGlass.actor;
        
        this._Main.layoutManager.disconnect(this._callbacks[0]);
        this._Main.layoutManager.disconnect(this._callbacks[1]);
        lookingGlass.disconnect(this._callbacks[2]);
        lookingGlass.disconnect(this._callbacks[3]);
        this._callbacks = []
    }
}