/**
 * Main Extension
 * 
 * @author      Advendra Deswanta <adeswanta@gmail.com>
 * @copyright   2021-2022
 * @license     GNU General Public License v3.0
 */

// Import ui library
const {
    main:Main,
    overviewControls:OverviewControls,
    workspaceThumbnail:WorkspaceThumbnail,
} = imports.ui;

// Import Gettext for translation
const Gettext = imports.gettext;

// Define current extension path as Self and import it's library
const Self = imports.misc.extensionUtils.getCurrentExtension();
const {
    misc:Misc,
    manager:Manager,
    schema:Schema,
    components:Components
} = Self.imports.library;

// Define current GNOME Shell version
let shellVersion = imports.misc.config.PACKAGE_VERSION;

var Extension = class {
    constructor() {}

    enable() {
        // Define extension metadata
        let metadata = Self.metadata;

        // Define install type variable to get installation type after installation
        let installedAsUser = (metadata['install-type'] == 'User') ? true : false;

        // Define settings schemas
        let shellSettings = Schema.newSchema('org.gnome.shell');
        let extensionPath = 'org.gnome.shell.extensions.shell-configurator';
        let extensionSettings = (installedAsUser) ?
            Schema.newSchemaFromSource(Self.dir.get_child('schema').get_path(), extensionPath) :
            Schema.newSchema(extensionPath);

        // Define the new Manager class
        this.Manager = new Manager.SCManager(
            Self, Main, {
                // Some GNOME Schemas:
                'shellSettings': shellSettings,
                'extensionSettings': extensionSettings
            }, {
                // Libraries:
                'Components': new Components.SCComponents(),
                'Misc': new Misc.SCMisc(Main, Self, shellSettings)
            }, parseFloat(shellVersion)
        );
    }

    disable() {
        this.Manager.destroy();

        // Make class variables empty (to null) in order not to make increasing memory
        this.Manager = null;
    }
}

function initTranslations() {
    if (parseFloat(shellVersion) >= 3.32) {
        imports.misc.extensionUtils.initTranslations()
    } else {
        // Forked from misc.extensionUtils.initTranslations()
        Gettext.bindtextdomain(
            Self.metadata['gettext-domain'], // Gettext Domain
            Self.dir.get_child("locale").get_path() // Locale directory
        );
        Object.assign(Self, Gettext.domain(Self.metadata['gettext-domain']));
    }
}

function init() {
    // Initialize translations
    initTranslations();

    return new Extension();
}