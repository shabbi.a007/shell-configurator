// Define current extension path and import it's library
const Self = imports.misc.extensionUtils.getCurrentExtension();
const { preferences, keys, misc, schema:SCSchema } = Self.imports.library;

// Import Gettext for translation
const Gettext = imports.gettext;

// Define current GNOME Shell version
const shellVersion = parseFloat(imports.misc.config.PACKAGE_VERSION);

// Define extension metadata
const SCMetadata = Self.metadata;

// Define install type variable to get installation type after installation
var installedAsUser = (SCMetadata['install-type'] == 'User') ? true : false;

// Define settings schemas
var extensionPath = 'org.gnome.shell.extensions.shell-configurator';
var extensionSettings =
    (installedAsUser)
    ?   SCSchema.newSchemaFromSource(Self.dir.get_child('schema').get_path(), extensionPath)
    :   SCSchema.newSchema(extensionPath);

// Define SCPrefs class
const Prefs = new preferences.SCPrefs(Self, {
    "Keys": new keys.SCKeys(),
    "Misc": new misc.SCMisc()
}, extensionSettings, shellVersion);

function initTranslations() {
    if (parseFloat(shellVersion) >= 3.32) {
        imports.misc.extensionUtils.initTranslations()
    } else {
        // Forked from misc.extensionUtils.initTranslations()
        Gettext.bindtextdomain(
            Self.metadata['gettext-domain'], // Gettext Domain
            Self.dir.get_child("locale").get_path() // Locale directory
        );
        Object.assign(Self, Gettext.domain(Self.metadata['gettext-domain']));
    }
}

function init() {
    // Initialize translations
    initTranslations();
}

function fillPreferencesWindow(window) {
    Prefs.buildUI(window);
}

function buildPrefsWidget() {
    // Build Prefs UI and return as content widget. See preferences.js on library
    return Prefs.buildUI();
}