# Shell Configurator
### Configure and customize GNOME Shell with advanced settings.

## Purpose
It use for add, remove, configure, and customize GNOME Shell with advanced
settings. You can configure it like your own.

## You can configure
1. Panel
2. Dash (or Dock)
3. Overview
4. Search 
6. Looking Glass 

For option references, read the
[REFERENCES.md](https://gitlab.com/adeswantaTechs/shell-configurator/-/blob/master/REFERENCES.md)

## This extension is supported by the following versions
| GNOME Version                | State          | Released Since      | Latest Version  | End Of Support |
| ---                          | ---            | ---                 | ---             | ---            |
| Older versions (3.30 - 3.34) | Released       | v1 - 7 May 2021     | v4 - 9 May 2021 | May 2023       |
| GNOME 3.36                   | Released       | v1 - 7 May 2021     | v4 - 9 May 2021 | May 2026       |
| GNOME 3.38                   | Released       | v1 - 7 May 2021     | v4 - 9 May 2021 | May 2026       |
| GNOME 40                     | Released       | v1 - 7 May 2021     | v4 - 9 May 2021 | June 2026      |
| GNOME 41                     | In development | v5 - N/A            | v5 - N/A        | TBA            |
| GNOME 42                     | In development | v5 - N/A            | v5 - N/A        | TBA            |

* **Release state process: Under Construction &rarr; In development &rarr; In review &rarr; Released**
* **After GS Version support reaches the end of support date, the release state will be "Unsupported"**

## Installation
You can install this extension with these methods:

### Install from GNOME Shell Extensions website
Go to [GNOME Shell Extensions Website](https://extensions.gnome.org/extension/4254/shell-configurator/),
then install/enable by toggling switcher on the top right.

### Install from GitLab Releases
Go to [Releases](https://gitlab.com/adeswantaTechs/shell-configurator/-/releases) 
and download the extension package on the Packages section

then, follow these commands:
```bash
# for GNOME 3.32 below:
$ mkdir -p "/home/$USER/.local/share/gnome-shell/extensions/shell-configurator@adeswanta/"
$ unzip "shell-configurator@adeswanta.shell-extension.zip" -d "/home/$USER/.local/share/gnome-shell/extensions/shell-configurator@adeswanta/"
# for GNOME 3.34 above:
$ gnome-extensions install --force 'shell-configurator@adeswanta.shell-extension.zip'
```

### Build from Source method
Before installing, you need install these packages:
* `git` for cloning repository.
* `zip` for packing and unpacking archive (for GNOME 3.32 below).
* `gnome-shell-extensions` for packing and unpacking extension archive
   (for GNOME 3.34 above).

then, follow these commands:
```bash
$ git clone https://gitlab.com/adeswantaTechs/shell-configurator.git
$ cd shell-configurator
$ ./installer.sh --build
# to install as user:
$ ./installer.sh --install USER
# or to install as system:
$ ./installer.sh --install SYSTEM
```

## Credits
This extension won't work better without these creators/references:
* **[GNOME Shell source code on Gitlab](https://gitlab.gnome.org/GNOME/gnome-shell)** for shell library references
* **[GJS Guide Website](https://gjs.guide)** for porting extension **[for GNOME 40 (with GTK 4)](https://gjs.guide/extensions/upgrading/gnome-shell-40.html)**, **[for GNOME 42](https://gjs.guide/extensions/upgrading/gnome-shell-42.html)** and **[Documentation](https://gjs-docs.gnome.org/)**
* **[Just Perfection's GNOME Shell Desktop Extension](https://gitlab.gnome.org/jrahmatzadeh/just-perfection)** for API reference and Prefs
* Edenhofer's **[Hide Workspace Thumbnails](https://extensions.gnome.org/extension/808/hide-workspace-thumbnails/)** for tweaking Workspace Thumbnails for GNOME 3.38 below from **[Minimalism-Gnome-Shell](https://github.com/Edenhofer/Minimalism-Gnome-Shell)**
* Thoma5's **[Bottom Panel](https://github.com/Thoma5/gnome-shell-extension-bottompanel)** for panel position and callbacks
* Selenium-H's **[App Grid Tweaks](https://github.com/Selenium-H/App-Grid-Tweaks)** for app grid configurations
* HROMANO's **[nohotcorner](https://github.com/HROMANO/nohotcorner/)** for disabling hot corners for GNOME Shell 3.32 below
* tuxor1337's **[Hide Top Bar](https://github.com/mlutfy/hidetopbar)** for auto hide top panel

## License
**Shell Configurator is licensed under The GNU General Public License v3.0. See [LICENSE](https://gitlab.com/adeswantaTechs/shell-configurator/-/blob/master/LICENSE)**
