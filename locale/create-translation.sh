if [ ! $1 ]; then
    echo "[Shell Configurator] [ERROR] Language is required"
    exit 1
fi

# Change current directory to locale folder from this repo root
#if [ "$(git rev-parse --show-toplevel)" == "$(pwd)" ]; then
cd "$(git rev-parse --show-toplevel)/locale"
#fi

msginit -i template.pot --locale $1 -o $1.po