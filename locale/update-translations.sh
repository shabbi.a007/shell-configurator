#!/bin/bash

xgettext \
    --from-code=UTF-8 \
    --copyright-holder="ADeswanta" \
    --package-name="Shell Configurator" \
    --package-version="5" \
    --output="locale/template.pot" \
    shell-configurator@adeswanta/library/*.js shell-configurator@adeswanta/ui/*.ui

for file in locale/*.po; do
    echo -n "Updating $(basename "$file" .po)"
    msgmerge -U "$file" locale/template.pot

    if grep --silent "#, fuzzy" "$file"; then
        fuzzy+=("$(basename "$file" .po)")
    fi
done

if [[ -v fuzzy ]]; then
    echo "Shell Configurator [WARNING]: Translations have unclear strings and need an update: ${fuzzy[*]}"
fi