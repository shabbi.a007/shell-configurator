# Contributing Shell Configurator Extension

### If you want to contribute our extension, you need follow these rules:
* Use **simple, readable, and easy to understand** code
* **Indentation** must be **4 spaces** not tabs. We recommended using
  **Visual Studio Code** and configure indentation to 4 spaces for coding
* Use **underscore symbol** at the first character as private functions/method
  or variable
  - example: `_workspaceSwitcher`
* Between methods should be **separated** with **new line**
* Don't use **longer code**, at least **minimum of 250 characters**
* Inside of `ui` directory includes **ui file** such as preferences and widgets
* Inside of `library` directory includes **extension library**
* Inside of `library/modules` directory includes some component **modules** that
  combined (programmatically) into **`library/components.js`**
* **`stylesheet.css`** only used for **tuning/improving ui elements** to make
  shell ui more stable


### If you want to add shell options with existing category, follow these instructions:
1.  Choose modules that you want to add at `library/modules/`*`module_name`*`.js`
2.  Write these codes below:
```js
var CategoryName = class {
    ...
    backup() {
        ...
        this._origin = {
            ...
            'option': option // origin value from Main or other shell components
        }
    }
    ...
    restore() {
        ...
        this.optionName(this._origin['option']);
    }
    ...
    // optionName: Method Description
    optionName() {
        // CODE GOES HERE
    }
    ...
}
```
**Rules:**
* Use function for configuration methods with comments to make methods easy to
  understand what this methods going on; ex:


### If you want to add shell configuration category, follow these instructions:
1.  Create module to the `library/modules/`*`module_name`*`.js`, then copy/write
    these codes below:
```js
var CategoryName = class {
    constructor() {
        super()
    }

    // backup: Backup shell properties
    backup() {
        super.backup();
        this._origin = {
            'optionOrigin': originValue // origin value from Main or other shell components
            // Add another configurations properties with default shell value
        }
    }

    // restore: Restore properties
    restore() {
        this.optionMethodName(this._origin['optionOrigin']);
        // Add another configurations method
    }

    // optionMethodName: Method Description
    optionMethodName() {
        // CODE GOES HERE
    }

    // _methodPrivate: Method Description
    _methodPrivate() {
        // CODE GOES HERE
    }
}
```
<small>*See `library/modules/_template.js` for references*</small>

**Rules:**
* `this._Misc` is only used for logging, sending errors, and other methods on
  misc.js
* `this._shellVersion` is only used if your option is only available for certain
  shell compatibility cases
  - example: TODO


### If you want to create another libraries, follow these instructions:
1.  Create module to the library/*library_name*.js, then copy/write these codes
    below:
```js
/**
 * // library name
 * 
 * @Description      // library category
 * @Filename         // library filename (ex: components.js)
 * @License          GNU General Public License v3.0
 */

// The rest of the code
```


### For text/variable naming cases use these cases:
* **CapitalCase** for Shell Configurator (SC) library
  - example: Components; library name: components.js
* **camelCase** for functions/methods, variables, and object properties
  - example: workspaceSwitcherScaleSize
* **PascalCase/CapitalCamelCase** for classes
  - example: SearchEntry