# Changelog

This is the notable changes of Shell Configurator Extension project.

## Version 5 - Whitesmoke (Unreleased) - N/A
The biggest major updates since 10 month of development and didn't released the
newer version

### **Added:**
* **Metadata** - GNOME 41 and 42 support
* **Library** - Looking Glass Module
* **Library** - Search Module
* **Library** - Notification Module
* **Preferences** - Preset feature
* **Extension** - Version codename, we use color name
* **Extension** - Translation files
* **Repo** - Contributing File
### **Enhanced:**
* **Preferences** - Option settings binding instead of connecting signals
### **Changed:**
* **Library** - Separated components modules
* **Library** - Move search options from "Overview" configuration to "Search"
* **Library** - Renamed module from overview to workspace
* **Preferences** - Rewritten and redesigned look with search feature
### **Fixed:**
* **Extension** - App grid doesn't change persistently after restart GNOME Shell
* **Prefs** - Uneditable SpinButton
### **Removed:**
* **Script** - Extension Updater, the extension updater are already built-in


## Version 4 (Technical Fixes) - 9 May 2021
Refreshed from Version 2 and 3 with some fixes

### **Changed:**
* **Extension** - Fixed Overview restoring workspace switcher scale size that
when disabling extension makes destroying the Shell overview
* **Repo** - Review file


## Version 3 (Technical Fixes) - 8 May 2021
Refreshed from Version 1 with some fixes

### **Changed:**
* **Extension** - Release state from system to user to make extension works when
installing on GNOME Shell Extension Website


## Version 2 (Technical Fixes) - 8 May 2021
Refreshed from Version 1 with some fixes

### **Added:**
* **Extension** - GSettings schemas compiled binary


## Version 1 (Initial Release) - 7 May 2021
Initial release of Shell Configurator

### **Added:**
* **Extension** - Extension Preferences
* **Library** - Top Panel Module
* **Library** - Dash Module
* **Library** - Overview Module
* **Library** - App Grid Module
* **Script** - Extension Installer (Build from Source Method)
* **Script** - Extension Updater
* **Metadata** - GNOME 3.36, 3.38, and 40 support
* **Metadata** - Older GNOME Versions (3.30 - 3.34) support
* **Repo** - Readme file
* **Repo** - References Documentation
* **Repo** - License file
* **Repo** - Changelog file
* **Repo** - To Do / Task file
* **Repo** - Versions Manager for updater